/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service;

import ch.ge.ve.interfaces.ech.eCH0045.v4.VoterDelivery;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamReader;

public class JAXBEchCodecVoterDelivery extends JAXBEchCodecImpl<VoterDelivery> {

  private final Validator validator;

  /**
   * Creates a new {@code JAXBEchCodecVoterDelivery} instance.
   */
  public JAXBEchCodecVoterDelivery() {
    super(VoterDelivery.class);
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Override
  public VoterDelivery deserialize(InputStream stream, boolean validate) {
    try {
      Unmarshaller unmarshaller = super.getContext().createUnmarshaller();
      if (validate) {
        unmarshaller.setSchema(EchSchema.get());
      }
      final VoterDelivery voterDelivery = VoterDelivery.class.cast(unmarshaller.unmarshal(stream));
      if (validate) {
        validateVoterDelivery(voterDelivery);
      }
      return voterDelivery;
    } catch (Exception e) {
      throw new EchDeserializationRuntimeException(e);
    }
  }

  @Override
  public VoterDelivery deserialize(XMLStreamReader streamReader, boolean validate) {
    try {
      Unmarshaller unmarshaller = super.getContext().createUnmarshaller();
      if (validate) {
        unmarshaller.setSchema(EchSchema.get());
      }
      final VoterDelivery voterDelivery = unmarshaller.unmarshal(streamReader, VoterDelivery.class).getValue();
      if (validate) {
        validateVoterDelivery(voterDelivery);
      }
      return voterDelivery;
    } catch (JAXBException e) {
      throw new EchDeserializationRuntimeException(e);
    }
  }

  @Override
  public void serialize(VoterDelivery ech, OutputStream stream, boolean validate) {
    try {
      Marshaller marshaller = super.getContext().createMarshaller();
      if (validate) {
        marshaller.setSchema(EchSchema.get());
        this.validateVoterDelivery(ech);
      }
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.marshal(ech, stream);
    } catch (JAXBException e) {
      throw new EchDeserializationRuntimeException(e);
    }
  }

  private void validateVoterDelivery(VoterDelivery voterDelivery) {
    final Set<ConstraintViolation<VoterDelivery>> violations = validator.validate(voterDelivery);
    if (!violations.isEmpty()) {
      throw new ValidationException("Error validating bean VoterDelivery.");
    }
  }
}
