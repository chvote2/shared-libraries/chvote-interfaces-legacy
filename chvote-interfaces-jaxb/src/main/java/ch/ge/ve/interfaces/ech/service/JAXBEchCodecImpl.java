/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service;

import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamReader;

/**
 * Implementation of {@code EchCodec} based on JAXB.
 *
 * @param <T> the type of objects being encoded/decoded.
 */
public class JAXBEchCodecImpl<T> implements EchCodec<T> {
  private final JAXBContext context;
  private final Class<T>    targetType;

  /**
   * Creates a new {@code JAXBEchCodecImpl} instance.
   *
   * @param targetType the Java class corresponding to the handled eCH.
   */
  public JAXBEchCodecImpl(Class<T> targetType) {
    try {
      this.context = JAXBContext.newInstance(targetType.getPackage().getName());
    } catch (JAXBException e) {
      throw new IllegalStateException("Initialization failure...", e);
    }
    this.targetType = targetType;
  }

  JAXBContext getContext() {
    return context;
  }

  @Override
  public T deserialize(InputStream stream, boolean validate) {
    try {
      Unmarshaller unmarshaller = context.createUnmarshaller();
      if (validate) {
        unmarshaller.setSchema(EchSchema.get());
      }
      return targetType.cast(unmarshaller.unmarshal(stream));
    } catch (JAXBException e) {
      throw new EchDeserializationRuntimeException(e);
    }
  }

  @Override
  public T deserialize(XMLStreamReader streamReader, boolean validate) {
    try {
      Unmarshaller unmarshaller = context.createUnmarshaller();
      if (validate) {
        unmarshaller.setSchema(EchSchema.get());
      }
      return unmarshaller.unmarshal(streamReader, targetType).getValue();
    } catch (JAXBException e) {
      throw new EchDeserializationRuntimeException(e);
    }
  }

  @Override
  public void serialize(T ech, OutputStream stream, boolean validate) {
    try {
      Marshaller marshaller = context.createMarshaller();
      if (validate) {
        marshaller.setSchema(EchSchema.get());
      }
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.marshal(ech, stream);
    } catch (JAXBException e) {
      throw new EchDeserializationRuntimeException(e);
    }
  }
}
