/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.parser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JAXB adapter to map date elements to LocalDateTime objects
 */
public class LocalDateTimeAdapter {

  private static final Logger logger = LoggerFactory.getLogger(LocalDateTimeAdapter.class);

  private LocalDateTimeAdapter() {
  }

  public static LocalDateTime unmarshal(String dateTime) {
    try {
      return LocalDateTime.parse(dateTime, DateTimeFormatter.ISO_DATE_TIME);
    } catch (DateTimeParseException ex) {
      logger.error(String.format("Could not parse date: %s", dateTime), ex);
      return null;
    }
  }

  public static String marshal(LocalDateTime dateTime) {
    return dateTime.format(DateTimeFormatter.ISO_DATE_TIME);
  }

}
