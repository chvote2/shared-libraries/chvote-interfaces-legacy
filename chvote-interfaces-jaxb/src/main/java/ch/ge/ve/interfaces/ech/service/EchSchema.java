/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * Static access to the global eCH schema definition.
 */
public final class EchSchema {

  private static final Schema SCHEMA = new EchSchema().createSchema();

  private final Logger logger = LoggerFactory.getLogger(EchSchema.class);

  // Instance of EchSchema is used as an internal builder to create the Schema object
  private EchSchema() { /* For internal use only */ }

  /**
   * Get access to the eCH {@link Schema} object. Regroup all XSD in a global schema definition.
   *
   * @return the global eCH constraints for the exposed XML beans
   */
  public static Schema get() {
    return SCHEMA;
  }

  /**
   * Convenient method to validate a eCH XML file against the XSD schemas.
   * <p>
   * If you need finer control over the validation, you can access the {@link Schema} object and the validators
   * <pre>EchSchema.get().newValidator()</pre>
   *
   * @param echFilePath path to the eCH XML file
   *
   * @throws IllegalArgumentException  if the given path cannot be read as an eCH source
   * @throws SchemaValidationException if the given eCH sources does not conform to the XSD schema definition
   */
  public static void validate(Path echFilePath) {
    try (InputStream inputStream = Files.newInputStream(echFilePath)) {
      SCHEMA.newValidator().validate(new StreamSource(inputStream));
    } catch (IOException e) {
      throw new IllegalArgumentException("Cannot read " + echFilePath, e);
    } catch (SAXException saxe) {
      throw new SchemaValidationException("Source is not XSD-valid : " + echFilePath, saxe);
    }
  }

  private Schema createSchema() {
    SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    try (Stream<StreamSource> sources = getSchemaSources()) {
      sf.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
      sf.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
      return sf.newSchema(sources.toArray(Source[]::new));
    } catch (SAXException e) {
      throw new IllegalStateException("Can't read eCH schemas...", e);
    }
  }

  private Stream<StreamSource> getSchemaSources() {
    final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    List<InputStream> inputStreamList = new ArrayList<>();
    // Caution: order matters here!
    // The dependency tree be must traversed depth first (imported schemas must be listed before).
    return Stream.of("xsd/eCH-0044-4-1.xsd", "xsd/eCH-0010-5-1.xsd", "xsd/eCH-0010-6-0.xsd", "xsd/eCH-0006-2-0.xsd",
                     "xsd/eCH-0008-3-0.xsd", "xsd/eCH-0007-5-0.xsd", "xsd/eCH-0007-6-0.xsd", "xsd/eCH-0135-1-0.xsd",
                     "xsd/eCH-0058-5-0.xsd", "xsd/eCH-0011-8-1.xsd", "xsd/eCH-0021-7-0.xsd", "xsd/eCH-0155-3-0.xsd",
                     "xsd/eCH-0155-4-0.xsd", "xsd/eCH-0222-1-0.xsd", "xsd/eCH-0110-4-0.xsd", "xsd/eCH-0157-4-0.xsd",
                     "xsd/eCH-0159-4-0.xsd", "xsd/eCH-0045-4-0.xsd", "xsd/eCH-0228-1-0.xsd",
                     "xsd/logistic-delivery.xsd", "xsd/counting-circle-1-0.xsd")
                 .peek(path -> logger.debug("Loading XSD definition from classpath : {}", path))
                 .map(classLoader::getResourceAsStream)
                 .peek(inputStreamList::add)    // register InputStream for onClose
                 .map(StreamSource::new)
                 .onClose(() -> closeAll(inputStreamList));
  }

  // Ensures all InputStream objects are closed, even if the call to close() throws an exception on a particular item
  private void closeAll(List<InputStream> streams) {
    logger.debug("{} InputStream objects must be closed", streams.size());

    final List<Exception> exceptions = new ArrayList<>(streams.size());
    for (InputStream inputStream : streams) {
      try {
        inputStream.close();
      } catch (Exception e) {
        logger.error("Failed to close an InputStream", e);
        exceptions.add(e);
      }
    }

    if (!exceptions.isEmpty()) {
      final IllegalStateException exception = new IllegalStateException(
          "Failed to close all InputStreams used to load the JAXB Schema - see suppressed exceptions and logs");
      exceptions.forEach(exception::addSuppressed);

      throw exception;
    }
  }
}
