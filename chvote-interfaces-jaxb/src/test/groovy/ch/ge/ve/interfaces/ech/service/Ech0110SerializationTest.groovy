/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service

import ch.ge.ve.interfaces.ech.eCH0110.v4.Delivery
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

import java.nio.charset.Charset

class Ech0110SerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0110-v4.xml"

  def factory = new JAXBEchCodecImpl<Delivery>(Delivery.class)

  def "a valid eCH-0110 file should be successfully parsed"() {
    given: 'a valid eCH-0110 XML file'
    InputStream stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when:
    Delivery delivery = factory.deserialize(stream)

    then: 'the file should be correctly parsed'
    delivery != null
    delivery.deliveryHeader.messageId == "5c0764e5ef9946658b3e10546a51885a"
    delivery.resultDelivery.contestInformation.contestIdentification == "201805 - VP du 06 mai 2018"
    delivery.resultDelivery.countingCircleResults.size() == 1
    delivery.resultDelivery.countingCircleResults.voteResults.size() == 1
  }

  def "a valid aggregated results delivery should be successfuly serialized to xml"() {
    given: 'a delivery generated from a valid eCH-0110 file'
    def file = SAMPLE_FILE_PATH
    Delivery delivery = factory.deserialize(getClass().getResourceAsStream(file))

    when: 'serializing the delivery read from the eCH-0110 file'
    OutputStream out = new ByteArrayOutputStream()
    factory.serialize(delivery, out)

    then: 'serialization output should be similar to the original file'
    Assert.assertThat(Input.from(new String(out.toByteArray(), Charset.forName("UTF-8"))),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(file))))
  }
}
