/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service

import ch.ge.ve.interfaces.ech.eCH0222.v1.Delivery
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

import java.nio.charset.Charset

class Ech0222SerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0222-v1.xml"

  def factory = new JAXBEchCodecImpl<Delivery>(Delivery.class)

  def "a valid eCH-0222 file should be successfully parsed"() {
    given: 'a valid eCH-0222 XML file'
    InputStream stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when:
    Delivery delivery = factory.deserialize(stream)

    then: 'the file should be correctly parsed'
    delivery != null
    delivery.deliveryHeader.messageId == "046210ff127c4cb0b00a530b844ffa93"
    def rawData = delivery.rawDataDelivery.rawData
    rawData.contestIdentification == "201805 - VP du 06 mai 2018"
    rawData.countingCircleRawData.size() == 1
    def countingCircleRawData = rawData.countingCircleRawData[0]
    countingCircleRawData.countingCircleId == "6621-2105"
    countingCircleRawData.voteRawData.size() == 1
    def voteRawData = countingCircleRawData.voteRawData[0]
    voteRawData.voteIdentification == "201805 VP-FED-CH"
    voteRawData.ballotRawData.size() == 2

    voteRawData.ballotRawData[0].ballotIdentification == "FED_CH_Q1"
    voteRawData.ballotRawData[0].ballotCasted.size() == 2
    voteRawData.ballotRawData[0].ballotCasted[0].ballotCastedNumber == 1
    voteRawData.ballotRawData[0].ballotCasted[0].questionRawData.size() == 1
    voteRawData.ballotRawData[0].ballotCasted[0].questionRawData[0].questionIdentification == "FED_CH_Q1"
    voteRawData.ballotRawData[0].ballotCasted[0].questionRawData[0].casted.castedVote == 1

    voteRawData.ballotRawData[1].ballotIdentification == "FED_CH_Q2"
    voteRawData.ballotRawData[1].ballotCasted.size() == 2
    voteRawData.ballotRawData[1].ballotCasted[0].ballotCastedNumber == 1
    voteRawData.ballotRawData[1].ballotCasted[0].questionRawData.size() == 3
    voteRawData.ballotRawData[1].ballotCasted[0].questionRawData[0].questionIdentification == "FED_CH_Q2.a"
    voteRawData.ballotRawData[1].ballotCasted[0].questionRawData[0].casted.castedVote == 1
    voteRawData.ballotRawData[1].ballotCasted[0].questionRawData[1].questionIdentification == "FED_CH_Q2.b"
    voteRawData.ballotRawData[1].ballotCasted[0].questionRawData[1].casted.castedVote == 2
  }

  def "a valid raw results delivery should be successfully serialized to xml"() {
    given: 'a delivery generated from a valid eCH-0222 file'
    def file = SAMPLE_FILE_PATH
    Delivery delivery = factory.deserialize(getClass().getResourceAsStream(file))

    when: 'serializing the delivery read from the eCH-0222 file'
    OutputStream out = new ByteArrayOutputStream()
    factory.serialize(delivery, out)

    then: 'serialization output should be similar to the original file'
    Assert.assertThat(Input.from(new String(out.toByteArray(), Charset.forName("UTF-8"))),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(file))))
  }
}
