# CHVote: Interfaces legacy
[![pipeline status](https://gitlab.com/chvote2/shared-libraries/chvote-interfaces-legacy/badges/master/pipeline.svg)](https://gitlab.com/chvote2/shared-libraries/chvote-interfaces-legacy/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-interfaces-legacy&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-interfaces-legacy)

A library containing the XSD and java bindings of the eCH standards needed in the CHVote project. This version is 
currently used in the CHVote project, but a new version is being developed in
a [new repository](https://gitlab.com/chvote2/shared-libraries/chvote-interfaces).

# Usage

## Add to your project

Maven:
```xml
<!-- CHVote interfaces core module -->
<dependency>
    <groupId>ch.ge.ve.interfaces</groupId>
    <artifactId>chvote-interfaces-jaxb</artifactId>
    <version>2.0.11</version>
</dependency>
```

# Building

## Pre-requisites

* JDK 8
* Maven

## Build steps

```bash
mvn clean install
```

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](https://gitlab.com/chvote2/shared-libraries/chvote-interfaces/blob/master/LICENSE) 
license.
